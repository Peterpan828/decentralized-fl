import torch
import copy

def model_sum(params1, params2):
	with torch.no_grad():
		for k in params1.keys():
			params1[k] += params2[k]

def model_avg(param_sum, param_count, args, h, origin):
	w_avg = copy.deepcopy(param_sum)

	with torch.no_grad():
		for k in w_avg.keys():
			if param_count[k] == 0:
				w_avg[k] = origin[k]
				continue
			
			w_avg[k] = torch.div(w_avg[k], param_count[k])

			if args.FedDyn == 1:
				h[k] = h[k] - args.alpha * param_count[k] * (w_avg[k] - \
					origin[k]) / args.nodes

				if 'weight' not in k and 'bias' not in k:
					continue
				
				w_avg[k] = w_avg[k] - h[k] / args.alpha

	return w_avg


# if __name__ == "__main__":
# 	import torch
# 	b = list()

# 	a = dict()
# 	a['1'] = torch.ones(2,2)
# 	a['2'] = torch.ones(2,2)
# 	b.append(a)

# 	a = dict()
# 	a['1'] = torch.ones(2,2)
# 	a['2'] = torch.ones(2,2)
# 	b.append(a)

# 	avg = b[0]
# 	for k in avg.keys():
# 		print(id(avg[k]))
# 		for i in range(1,len(b)):
# 			avg[k] = avg[k] + b[i][k]
# 		print(id(avg[k]))
# 		#avg[k] = torch.div(avg[k], len(b))

# 	print(avg)
		