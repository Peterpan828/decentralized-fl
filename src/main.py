import torch
import numpy as np
import time
import arguments
import copy
import random
import os
import torch.multiprocessing as mp
from setproctitle import setproctitle
import queue

from node import Client
from server import Server
from workers import *
from dataLoader.dataLoaders import getAllDataLoaders

# for random resource capability distribution experiments

def random_comp():
    
    comp = list()
    for i in range(4):
        comp.append(random.uniform(0,1))
    
    total_sum = sum(comp)

    for i in range(4):
        comp[i] /= total_sum

    return comp

# allocate local model considering resource capability

def select_level(id, args):
    if args.randomBranch == 0:
        i = id / args.nodes
    else:
        i = np.random.random_sample()
    if i < args.level1:
        return 0
    elif i < (args.level1+args.level2):
        return 1
    elif i < (args.level1 + args.level2 + args.level3):
        return 2
    else:
        return 3

if __name__ == "__main__":
    mp.set_start_method('spawn')
    setproctitle("Main Process")
    if torch.cuda.is_available():
        n_devices = torch.cuda.device_count()
        devices = [torch.device("cuda:{}".format(i)) for i in range(n_devices)]
        cuda = True
        #print('학습을 진행하는 기기:', devices)
    else:
        n_devices = 1
        devices = [torch.device('cpu')]
        cuda = False
        #print('학습을 진행하는 기기: CPU')

    os.environ["OMP_NUM_THREADS"] = "1"

    args = arguments.parser()
    
    # Full experiment
    if args.full == 1:
        args.level1 = 0.0
        args.level2 = 0.0
        args.level3 = 0.0
        args.level4 = 1.0

    # Dynamic experiment
    elif args.interpolation == 1:
        comp = random_comp()
        args.level1 = comp[0]
        args.level2 = comp[1]
        args.level3 = comp[2]
        args.level4 = comp[3]

    num_nodes = args.nodes
    num_round = args.round
    num_local_epoch = args.local_epoch
    
    print("> Setting:", args)

    nodeIDs = [i for i in range(num_nodes)]
    nodeindices, testLoader = getAllDataLoaders(nodeIDs, num_nodes, args)
    n_train_processes = n_devices * args.n_procs
    trainIDs = ["Train Worker : {}".format(i) for i in range(n_train_processes)]
    trainQ = mp.Queue()
    resultQ = mp.Queue()
    testQ = mp.Queue()
    preQ = queue.Queue()

    # create test process
    processes = []
    p = mp.Process(target=gpu_test_worker, args=(testLoader, testQ, devices[0], args))
    p.start()
    processes.append(p)
    time.sleep(0.1)

    # create train processes
    for i, trainID in enumerate(trainIDs):
        p = mp.Process(target=gpu_train_worker, args=(trainID, trainQ, resultQ, devices[i%n_devices], args))
        p.start()
        processes.append(p)
        time.sleep(0.1)

    # create pseudo server
    server = Server(args)
    server.set_initial_model()

    # for FedDyn optimizer
    prev_grads = {}
    with torch.no_grad():
        prev_grads = {k: torch.zeros(v.numel()) for (k, v) in server.models[-1].named_parameters()}

    # create pseudo clients
    nodes = []
    for i, nodeID in enumerate(nodeIDs):
        nodes.append(Client(nodeID, nodeindices[nodeID], copy.deepcopy(prev_grads), args))
    
    lr = args.lr

    # initialize h value for FedDyn optimizer
    server.initialize_h()

    np.random.seed(43)
    
    for roundIdx in range(args.round+1)[1:]:
        
        if roundIdx % 5 == 0:
            print(f"Round {roundIdx}", end=', ')
            
        cur_time = time.time()
        lr *= args.lr_decay
        current = np.clip(roundIdx, 0.0, args.consistency_rampup)
        phase = 1.0 - current / args.consistency_rampup
        consistency_weight = float(np.exp(-5.0 * phase * phase))

        # Randomly selected clients
        n_trainees = int(num_nodes*args.fraction)
        trainees = [nodes[i] for i in np.random.choice(np.arange(num_nodes), n_trainees, replace=False)]
        
        count = 0

        for i, node in enumerate(trainees):
            
            # allocate local model considering resource capability
            branch_num = select_level(node.nodeID, args)

            # for naive baselines
            if args.base != -1:
                if branch_num < args.base:
                    continue
                else:
                    branch_num = args.base
            
            # download model
            model = server.get_branch_model(branch_num)
            count += 1
            trainQ.put({'type': 'train', 'node': copy.deepcopy(node), 'lr':lr, \
                'consistency_weight':consistency_weight, 'model': copy.deepcopy(model)})
        
        for _ in range(count):
            msg = resultQ.get()
            weight = msg['weight']
            node_id = msg['id']
            node = nodes[node_id]

            # set prev_grads for FedDyn optimizer
            if args.FedDyn == 1:
                prev_grads = msg['prev_grads']
                node.set_prev_grads(prev_grads)

            # upload weights to server
            server.update_node_info(weight)
            del msg
        
        # aggregate uploaded weights
        server.avg_parameters()

        if roundIdx % 5 == 0:
            if args.interpolation == 0:
                preQ.put({'round': roundIdx, 'model': copy.deepcopy(server.models[-1])})
            elif roundIdx >= 890:
                preQ.put({'round': roundIdx, 'model': copy.deepcopy(server.models[-1])})

            print(f"Elapsed Time : {time.time()-cur_time:.1f}")

    for _ in range(n_train_processes):
        trainQ.put('kill')

    # Train finished
    time.sleep(5)
    # Test start

    if args.interpolation == 0:
        for i in range(preQ.qsize()):
            testQ.put(preQ.get())

    else:
        for i in range(preQ.qsize()):
            msg = preQ.get()
            msg['levels'] = [args.level1, args.level2, args.level3, args.level4]

            testQ.put(msg)

    testQ.put('kill')

    for p in processes:
        p.join()
