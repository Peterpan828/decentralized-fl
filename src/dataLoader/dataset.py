from torch.utils.data import DataLoader
from torchvision import datasets, transforms

# def get_femnistDatasets(n_nodes):
#     apply_transform_train = transforms.Compose(
#         [
#             transforms.CenterCrop((96,96)),
#         transforms.Grayscale(num_output_channels=1),
#             transforms.Resize((28,28)),
#             transforms.ColorJitter(contrast=3),
#             transforms.ToTensor(),
#             transforms.Normalize((0.1307,), (0.3081,))]
#     )
#     total_nodes = len(os.listdir("../data/emnist/writer"))

#     train_datasets = []
#     test_datasets = []
#     for nodeIdx in range(total_nodes):
#         directory = '../data/emnist/writer/{}'.format(nodeIdx)
#         dataset = datasets.ImageFolder(directory, transform=apply_transform_train)
#         test_dataset , train_dataset = torch.utils.data.random_split(dataset, [3, len(dataset)-3], generator=torch.Generator().manual_seed(42))
#         train_datasets.append(train_dataset)
#         test_datasets.append(test_dataset)

#     test_dataset = torch.utils.data.ConcatDataset(test_datasets)

#     #print(len(test_dataset))

#     total_sample = []
    
#     for dataset in train_datasets:
#         total_sample.append(len(dataset))
    
#     #print("Total : {}".format(sum(total_sample)))
#     #print("Mean  : {}".format(sum(total_sample) / len(train_datasets)))
#     #print("Min   : {}".format(min(total_sample)))
#     #print("Max   : {}".format(max(total_sample)))
#     #exit()

#     return train_datasets, test_dataset


def get_dataset(args):
    
    if args.dataset == 'cifar10':
        apply_transform_train = transforms.Compose(
            [transforms.RandomCrop(32, padding=4),
            transforms.RandomHorizontalFlip(),
             transforms.ToTensor(),
             transforms.Normalize((0.4914, 0.4822, 0.4465),
                                  (0.2470, 0.2435, 0.2616)),
                                  ]
        )
        apply_transform_test = transforms.Compose(
            [transforms.ToTensor(),
             transforms.Normalize((0.4914, 0.4822, 0.4465),
                                  (0.2470, 0.2435, 0.2616)),
                                  ]
        )
        dir = '~/decentralized-fl/data/cifar'
        train_dataset = datasets.CIFAR10(dir, train=True, download=True,
                                         transform=apply_transform_train)
        test_dataset = datasets.CIFAR10(dir, train=False, download=True,
                                        transform=apply_transform_test)
        return train_dataset, test_dataset

    if args.dataset == 'cifar100':
        apply_transform_train = transforms.Compose(
            [transforms.RandomCrop(32, padding=4),
            transforms.RandomHorizontalFlip(),
             transforms.ToTensor(),
             transforms.Normalize((0.5071, 0.4867, 0.4408),
                                  (0.2675, 0.2565, 0.2761)),
                                  ]
        )
        apply_transform_test = transforms.Compose(
            [transforms.ToTensor(),
             transforms.Normalize((0.5071, 0.4867, 0.4408),
                                  (0.2675, 0.2565, 0.2761)),
                                  ]
        )
        dir = '~/decentralized-fl/data/cifar100'
        train_dataset = datasets.CIFAR100(dir, train=True, download=True,
                                         transform=apply_transform_train)
        test_dataset = datasets.CIFAR100(dir, train=False, download=True,
                                        transform=apply_transform_test)

        return train_dataset, test_dataset

    if args.dataset == 'tiny-imagenet':
        apply_transform_train = transforms.Compose(
            [transforms.RandomRotation(20),
            transforms.RandomHorizontalFlip(0.5),
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.4802, 0.4481, 0.3975],
                                std=[0.2302, 0.2265, 0.2262])
                                  ]
        )
        apply_transform_test = transforms.Compose(
            [transforms.ToTensor(),
            transforms.Normalize(mean=[0.4802, 0.4481, 0.3975],
                                std=[0.2302, 0.2265, 0.2262])
                                  ]
        )
        dir = '~/decentralized-fl/data/tiny-imagenet/'
        train_dataset = datasets.ImageFolder(dir+'train', transform=apply_transform_train)
        test_dataset = datasets.ImageFolder(dir+'test', transform=apply_transform_test)

        return train_dataset, test_dataset

    # if args.dataset == 'mnist':
    #     apply_transform_train = transforms.Compose(
    #         [
    #          transforms.ToTensor(),
    #          transforms.Normalize((0.1307,), (0.3081,))]
    #     )
    #     apply_transform_test = transforms.Compose(
    #         [transforms.ToTensor(),
    #          transforms.Normalize((0.1307,), (0.3081,))]
    #     )
    #     dir = '~/decentralized-fl/data/mnist'
    #     train_dataset = datasets.MNIST(dir, train=True, download=True,
    #                                      transform=apply_transform_train)
    #     test_dataset = datasets.MNIST(dir, train=False, download=True,
    #                                     transform=apply_transform_test)
    
    #     return train_dataset, test_dataset


if __name__ == "__main__":

    import argparse

    def parser():
        parser = argparse.ArgumentParser(description='Some hyperparameters')
        parser.add_argument('--dataset',  type=str, default='tiny-imagenet',
                        help='type of dataset')
        
        args = parser.parse_args()
        return args

    args = parser()
    train_dataset, test_dataset = get_dataset(args)

    train_loader = DataLoader(train_dataset, batch_size = 1, shuffle = True)
    test_loader = DataLoader(test_dataset, batch_size = 1, shuffle = False)

    train_batch = train_dataset[0]

    print(train_batch)
   
