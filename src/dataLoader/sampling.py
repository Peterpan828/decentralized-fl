import numpy as np
import torch
import matplotlib.pyplot as plt
from torchvision import datasets, transforms
from torch.utils.data import Dataset, DataLoader

# class Split(Dataset):
#     def __init__(self, dataset, index):
#         self.dataset = dataset
#         self.index = index

#     def __len__(self):
#         return len(self.index)

#     def __getitem__(self, item):
#         image, label = self.dataset[self.index[item]]
#         return torch.tensor(image), torch.tensor(label)

def iid(dataset, num_nodes):
    np.random.seed(41)
    num_sample = int(len(dataset)/(num_nodes))
    dict_nodes = {}
    index = [i for i in range(len(dataset))]
    for i in range(num_nodes):
        dict_nodes[i] = np.random.choice(index, num_sample,
                                         replace=False)
        index = list(set(index)-set(dict_nodes[i]))
    return dict_nodes

def noniid(dataset, num_nodes, beta):
    labels = np.array([label for _, label in dataset])
    min_size = 0
    K = np.max(labels) + 1
    N = labels.shape[0]
    net_dataidx_map = {}
    n_nets = num_nodes
    np.random.seed(31)

    while min_size < 10:
        idx_batch = [[] for _ in range(n_nets)]
        # for each class in the dataset
        for k in range(K):
            idx_k = np.where(labels == k)[0]
            np.random.shuffle(idx_k)
            proportions = np.random.dirichlet(np.repeat(beta, n_nets))
            ## Balance
            proportions = np.array([p*(len(idx_j)<N/n_nets) for p,idx_j in zip(proportions,idx_batch)])
            proportions = proportions/proportions.sum()
            proportions = (np.cumsum(proportions)*len(idx_k)).astype(int)[:-1]
            idx_batch = [idx_j + idx.tolist() for idx_j,idx in zip(idx_batch,np.split(idx_k,proportions))]
            min_size = min([len(idx_j) for idx_j in idx_batch])

    for j in range(n_nets):
        np.random.shuffle(idx_batch[j])
        net_dataidx_map[j] = np.array(idx_batch[j])
        #print(len(net_dataidx_map[j]))

    return net_dataidx_map


# def index_biased(dataset):

#     trainloader = DataLoader(dataset, batch_size=1, shuffle=False)

#     index = dict()

#     for i in range(10):
#         index['{}'.format(i)] = list()

#     for batch_idx, (example_data, example_targets) in enumerate(trainloader):
#         for i in range(10):
#             if example_targets == i:
#                 index['{}'.format(i)].append(batch_idx)

#     return index


# def index_noniid(dataset, biased_index):

#     index_biased = biased_index
#     ids = dict()
    
#     for i in range(5):
#         temp = index_biased[str(i*2)]
#         for _ in range(1):
#             temp.extend(index_biased[str(i*2+1)])
#         ids[str(i)] = temp
        
#     return ids


# if __name__ == "__main__":

#     dir = '~/decentralized-fl/data/mnist'
#     apply_transform = transforms.Compose([
#     transforms.ToTensor(),
#     transforms.Normalize((0.1307,), (0.3081,))
#     ])

#     train_dataset = datasets.MNIST(dir, train=True, download=True,
#                                 transform=apply_transform)

#     # Show bias dataset

#     biased_index = index_biased(train_dataset)
#     ids = index_noniid(train_dataset, biased_index)
#     for i in range(len(ids)):
#         print(len(ids[str(i)]))

#     trainloader_3 = DataLoader(Split(train_dataset, ids['0']),
#                             batch_size=6, shuffle=True)

#     examples = enumerate(trainloader_3)
#     batch_idx, (example_data, example_targets) = next(examples)

#     fig = plt.figure()
#     for i in range(6):
#         plt.subplot(2, 3, i+1)
#         plt.tight_layout()
#         plt.imshow(example_data[i][0].clone().detach(), cmap='gray')
#         plt.title("Ground Truth: {}".format(example_targets[i]))
#         plt.xticks([])
#         plt.yticks([])
#         plt.show()


#     trainloader_3 = DataLoader(Split(train_dataset, ids['4']),
#                             batch_size=6, shuffle=True)

#     examples = enumerate(trainloader_3)
#     batch_idx, (example_data, example_targets) = next(examples)

#     fig = plt.figure()
#     for i in range(6):
#         plt.subplot(2, 3, i+1)
#         plt.tight_layout()
#         plt.imshow(example_data[i][0].clone().detach(), cmap='gray')
#         plt.title("Ground Truth: {}".format(example_targets[i]))
#         plt.xticks([])
#         plt.yticks([])
#         plt.show()
