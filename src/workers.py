import copy
import pickle
import os
from setproctitle import setproctitle

from models import evaluate
from dataLoader.dataset import get_dataset


def mv_average_10(lst):
    mv_lst = []
    for i in range(len(lst)-10):
        mv_lst.append(round(sum(lst[i:i+10]) / 10, 4))
    return mv_lst


def mv_average_20(lst):
    mv_lst = []
    for i in range(len(lst)-20):
        mv_lst.append(round(sum(lst[i:i+20]) / 20, 4))
    return mv_lst


def gpu_train_worker(trainID, trainQ, resultQ, device, args):

	setproctitle(trainID)
	train_dataset, _ = get_dataset(args)

	while True:
		msg = trainQ.get()

		if msg == 'kill':
			break

		elif msg['type'] == 'train':
			
			processing_node = msg['node']
			model = msg['model']
			model_weight = processing_node.train(device, msg['lr'], msg['consistency_weight'], model, train_dataset)
			result = {'weight':copy.deepcopy(model_weight), 'id':processing_node.nodeID}
			
			if args.FedDyn == 1:
				result['prev_grads'] = copy.deepcopy(processing_node.get_prev_grads())
			else:
				result['prev_grads'] = None
			resultQ.put(result)
		del processing_node
		del model
		del model_weight
		del msg

	#print("train end")

def gpu_test_worker(test_loader, testQ, device, args):
	setproctitle("Test Worker : {}".format(device))

	global_acc = {'multi':list(), 'single':list()}
	
	while True:
		msg = testQ.get()

		if msg == 'kill':
			break

		else:
			if args.interpolation == 1:
				levels = msg['levels']

			model = msg['model']
			round = msg['round']
			
			acc_multi, acc_single, loss = evaluate(model, test_loader, args, device)
			global_acc['multi'].append(acc_multi)
			global_acc['single'].append(acc_single)
		
			print("Round: {} / Multi Accuracy: {}".format(round, acc_multi))
			print("Round: {} / Single Accuracy: {}".format(round, acc_single[0]))

	if args.interpolation == 1:
		file_name = '../save/{}/iid[{}]_K[{}].pkl'.format(args.dataset, args.iid, args.kd)

		if args.iid == 1:
			mv_acc = mv_average_10(global_acc['multi'])
		else:
			mv_acc = mv_average_20(global_acc['multi'])

		if(os.path.exists(file_name)):
			with open(file_name, 'rb') as f:
				interpolation_dict = pickle.load(f)
			
			interpolation_dict['levels'].append(levels)
			interpolation_dict['accuracy'].append(mv_acc[-1])

		else:
			interpolation_dict = {}
			interpolation_dict['levels'] = [levels]
			interpolation_dict['accuracy'] = [mv_acc[-1]]

		with open(file_name, 'wb') as f:
			pickle.dump(interpolation_dict, f)


	else:
		if args.base == -1:
			file_name = '../save/{}/N[{}]_iid[{}]_K[{}]_F[{}]_Fe[{}]_B[{}]_P[{},{},{},{}]_decay[{}]_R[{}]_norm[{}]_RB[{}]_no[{}]_FR[{}]_Ba[{}]_main_4.pkl'.\
				format(args.dataset, args.nodes, \
					args.iid, args.kd, args.FedDyn, args.feature, args.feature_beta, \
						args.level1, args.level2, args.level3, args.level4, args.lr_decay, args.consistency_rampup, args.norm, args.randomBranch, args.removeBranch, args.fraction, args.base)
		else:
			file_name = '../save/{}/N[{}]_R[{}]_iid[{}]_K[{}]_F[{}]_B[{}]_no[{}]_base_4.pkl'.\
				format(args.dataset, args.nodes, args.round, \
					args.iid, args.kd, \
						args.FedDyn, args.base, args.norm)

		with open(file_name, 'wb') as f:
			pickle.dump([global_acc], f)
		
		#print("test end")
