#!/bin/bash

for i in {1..13}; do python main.py --dataset='cifar10' --interpolation=1; done
for i in {1..12}; do python main.py --dataset='cifar100' --interpolation=1; done
for i in {1..10}; do python main.py --dataset='cifar10' --interpolation=1 --iid=0; done
for i in {1..10}; do python main.py --dataset='cifar100' --interpolation=1 --iid=0; done