import argparse


def parser():
    parser = argparse.ArgumentParser(description='Some hyperparameters')

    parser.add_argument('--nodes', type=int, default=100,
                        help='total number of nodes')
    parser.add_argument('--num_branch', type=int, default=4,
                        help='number of high level branches')
    parser.add_argument('--fraction', type=float, default=0.1,
                        help='ratio of participating node')
    parser.add_argument('--round', type=int, default=1000,
                        help='number of rounds')
    parser.add_argument('--local_epoch',  type=int, default=5,
                        help='number of local_epoch')
    parser.add_argument('--dataset',  type=str, default='cifar10',
                        help='type of dataset')
    parser.add_argument('--batch_size', type=int, default=50, 
                        help='size of batch')
    parser.add_argument('--lr', type=float, default=0.1,
                        help='learning rate')
    parser.add_argument('--lr_decay', type=float, default=0.998,
                        help='0.992, 0.998')
    parser.add_argument('--iid', type=int, default=1,
                        help='iid')
    parser.add_argument('--quantization', type=int, default=32,
                        help='quantization bit')
    parser.add_argument('--temperature', type=float, default=1.,
                        help='temperature of distillation')
    parser.add_argument('--pruning', type=int, default=1,
                        help='whether pruning')
    parser.add_argument('--n_procs', type=int, default=3,
                        help='number of processes')
    parser.add_argument('--kd', type=int, default=1,
                        help='whether use knowledge distillation')
    parser.add_argument('--feature', type=int, default=0,
                        help='whether use feature loss')
    parser.add_argument('--FedDyn', type=int, default=1,
                        help='whether FedDyn Algorithm')
    parser.add_argument('--alpha', type=int, default=0.1,
                        help='alpha for FedDyn')
    parser.add_argument('--consistency_rampup', type=int, default=300,
                        help='consistency_rampup')
    parser.add_argument('--beta', type=int, default= 0.5,
                        help='beta for non iid dirichlet dist')
    parser.add_argument('--full', type=int, default= 0,
                        help='All Full clients')
    parser.add_argument('--ls', type=float, default= 0.1,
                        help='Whether using label smoothing')
    parser.add_argument('--kd_alpha', type=float, default= 0.3,
                        help='alpha')
    parser.add_argument('--feature_beta', type=float, default= 0.03,
                        help='beta')
    parser.add_argument('--base', type=int, default=-1,
                        help='default : -1')
    parser.add_argument('--norm', type=str, default='bn',
                        help='Default: Batch Normalization')
    parser.add_argument('--randomBranch', type=int, default=0,
                        help='Dynamic Experiment')
    parser.add_argument('--level1', type=float, default=0.25,
                        help='d_k = 1')
    parser.add_argument('--level2', type=float, default=0.25,
                        help='d_k=2')
    parser.add_argument('--level3', type=float, default=0.25,
                        help='d_k=3')
    parser.add_argument('--level4', type=float, default=0.25,
                        help='d_k=4')
    parser.add_argument('--interpolation', type=int, default=0,
                        help='whether interpolation experiment')
    parser.add_argument('--scale', type=int, default=1,
                        help='whether using scale layer')
    parser.add_argument('--inter_ratio', type=float, default=1,
                        help='HeteroFL interpolation ratio')
    parser.add_argument('--sub_model', type=str, default='e',
                        help='HeteroFL experiment submodel')
                        
    args = parser.parse_args()
    return args


if __name__ == "__main__":
    args = parser()
    print(args)
