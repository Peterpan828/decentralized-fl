import torch
import torch.nn as nn
import torch.nn.functional as F
import copy
from torch.utils.data import DataLoader, Subset

class KLLoss(nn.Module):
    def __init__(self, args):
        self.args = args
        super(KLLoss, self).__init__()

    def forward(self, pred, label, KD=True):
        T=self.args.temperature
        
        predict = F.log_softmax(pred/T,dim=1)
        target_data = F.softmax(label/T,dim=1)
        target_data =target_data+10**(-7)
        with torch.no_grad():
            target = target_data.detach().clone()

        loss=T*T*((target*(target.log()-predict)).sum(1).sum()/target.size()[0])
        return loss


class Client:
    def __init__(self, nodeID, node_indices, prev_grads, args):
        self.nodeID = nodeID
        self.__node_indices = node_indices
        self.__args = args
        if args.FedDyn == 1:
            self.__prev_grads = prev_grads

    def get_prev_grads(self):
        return copy.deepcopy(self.__prev_grads)

    def set_prev_grads(self, prev_grads):
        self.__prev_grads = prev_grads
    
    def train(self, device, lr, consistency_weight, model, train_dataset):
        
        train_loader = DataLoader(Subset(train_dataset, self.__node_indices), \
            batch_size=self.__args.batch_size, shuffle=True)

        model.train()
        model.to(device)

        if self.__args.FedDyn == 1:
            with torch.no_grad():
                server_params = {k: param.flatten().clone() for (k, param) in model.named_parameters()}

            for k in self.__prev_grads.keys():
                self.__prev_grads[k] = self.__prev_grads[k].to(device)
        
        optimizer = torch.optim.SGD(model.parameters(), lr=lr, weight_decay=1e-3)
        criterion = nn.CrossEntropyLoss()
        criterion_kl = KLLoss(self.__args).cuda()

        for _ in range(self.__args.local_epoch):
            for _, (images, labels) in enumerate(train_loader):
                images, labels = images.to(device), labels.to(device)
                loss = 0.
                optimizer.zero_grad()
                output_list, feature_list = model(images)

                ensemble_output = torch.stack(output_list, dim=2)
                ensemble_output = torch.sum(ensemble_output, dim=2) / len(output_list)
                
                # loss += criterion(ensemble_output, labels) ## true label, ensemble output loss
     
                for i, (branch_output, branch_feature) in enumerate(zip(output_list, feature_list)):
                    loss += criterion(branch_output, labels) ## true label, branch output loss
                    
                    if len(output_list) > 1:
                        if self.__args.kd == 1:
                            for j in range(len(output_list)):
                                if j == i:
                                    continue
                                else:
                                    loss += consistency_weight * criterion_kl(branch_output, output_list[j].detach()) / (len(output_list) - 1) ## Self distillation term


                if self.__args.FedDyn == 1:
                    for k, param in model.named_parameters():
                        curr_param = param.flatten()
                        ## linear penalty

                        lin_penalty = torch.dot(curr_param, self.__prev_grads[k])
                        loss -= lin_penalty

                        ## quadratic penalty
                        
                        quad_penalty = self.__args.alpha/2.0 * torch.sum(torch.square(curr_param - server_params[k]))
                        loss += quad_penalty

                loss.backward()
                optimizer.step()

        if self.__args.FedDyn == 1:
            # update prev_grads
            with torch.no_grad():
                for k, param in model.named_parameters():
                    curr_param = param.flatten().clone()
                    self.__prev_grads[k] = self.__prev_grads[k] - self.__args.alpha * (curr_param - server_params[k])
                    self.__prev_grads[k] = self.__prev_grads[k].to(torch.device('cpu'))
        
        model.to(torch.device('cpu'))
        weight = model.state_dict()

        return copy.deepcopy(weight)