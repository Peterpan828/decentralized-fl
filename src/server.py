import torch

from nn_models.resnet import multi_resnet18_kd
from nn_models.resnet_tiny import multi_resnet18_tiny
from nn_models.vggnet import make_VGG
from model_avg import model_avg

class Server:
    def __init__(self, args):
        self.models = []
        self.param_sum = dict()
        self.param_count = dict()
        self.args = args
        self.h = dict() ## per each branch

    def get_branch_model(self, branch_num):
        return self.models[branch_num]

    def initialize_h(self):
        for k, v in self.models[-1].state_dict().items():
            self.h[k] = torch.zeros(v.shape)

    def update_node_info(self, weight):
    
        for k in weight.keys():
            if self.param_sum[k] is None:
                self.param_sum[k] = weight[k]
            else:
                self.param_sum[k] += weight[k]
            self.param_count[k] += 1
            
    def avg_parameters(self):
        origin = self.models[-1].state_dict()
        avg_parameters = model_avg(self.param_sum, self.param_count, self.args, self.h, origin)
        
        for model in self.models:
            state_dict = {k: avg_parameters[k] for k in model.state_dict().keys()}
            model.load_state_dict(state_dict)
        
        for k in self.models[-1].state_dict().keys():
            self.param_sum[k] = None
            self.param_count[k] = 0

    def set_initial_model(self):

        if self.args.dataset == 'cifar10':
            num_classes = 10
            if self.args.base == -1:
                for i in range(self.args.num_branch):
                    self.models.append(make_VGG(n_blocks=i+1, norm=self.args.norm))
        
            else:
                for i in range(self.args.base+1):
                    self.models.append(make_VGG(n_blocks=i+1, norm=self.args.norm))

        elif self.args.dataset == 'cifar100':
            num_classes = 100
            if self.args.base == -1:
                for i in range(self.args.num_branch):
                    self.models.append(multi_resnet18_kd(n_blocks=i+1, num_classes=num_classes, norm=self.args.norm))
        
            else:
                for i in range(self.args.base+1):
                    self.models.append(multi_resnet18_kd(n_blocks=i+1, num_classes=num_classes, norm=self.args.norm))
        
        elif self.args.dataset == 'tiny-imagenet':
            num_classes = 200
            if self.args.base == -1:
                for i in range(self.args.num_branch):
                    self.models.append(multi_resnet18_tiny(n_blocks=i+1, num_classes=num_classes, norm=self.args.norm))
        
            else:
                for i in range(self.args.base+1):
                    self.models.append(multi_resnet18_tiny(n_blocks=i+1, num_classes=num_classes, norm=self.args.norm))


        self.param_sum = {k: None for k in self.models[-1].state_dict().keys()}
        self.param_count = {k: 0 for k in self.models[-1].state_dict().keys()}

        for model in self.models:
            state_dict = {k: self.models[-1].state_dict()[k] for k in model.state_dict().keys()}
            model.load_state_dict(state_dict)




